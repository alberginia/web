---
layout: default
title: "Inscriu-te al SobTec"
---

# Inscriu-te si vols venir!

El SobTec és gratuït, però poder preveure quanta gent vindrà ens facilita molt les coses que la gent s'apunti. En cas que se superés l'aforament d'una sala, es donaria prioritat a les persones inscrites.
<div style="text-align:center">
<a href="https://cloud.hangar.org/apps/forms/s/GMxXooyDcRRSoCftaQmtGtt2" class="button">Inscriu-te al SobTec 2024</a>
</div>

# Vols col·laborar amb nosaltres?

Hi ha moltes maneres de col·laborar amb el SobTec {{site.year}}. Aquí en proposem algunes, però estem oberts a qualsevol proposta:

## Ajuda'ns a fer possible el SobTec {{site.year}}!
Els dies abans, durant i després del SobTec és quan hi ha més feina i més ajuda necessitem. Per organitzar-nos, repartim les tasques en grups: Apuntat al que vulguis!

* **Logística:**
Ens ajuda a muntar i desmuntar l'espai, moure taules i cadires i preparar la part audiovisual. La part grossa de la feina serà **dijous matí i/o tarda, divendres tarda, diumenge tarda i/o dilluns al matí**. Clar que si cada persona que ve ens ajuda una mica, acabarem abans...  
Pot apuntar-te a la logística en [aquest formulari](https://cloud.hangar.org/apps/polls/s/0nzB1oqF)

* **Prendre apunts amb la teixidora:**
Porta el teu ordinador i pren apunts col·laboratius de les xerrades, que després pujarem a [La Teixidora](https://www.teixidora.net/). A la presentació i després d'algunes pauses explicarem com fer-ho, però és molt fàcil i divertit... ah, i no cal tenir un català perfecte ;)  
Pot apuntar-te a prendre apunts en [aquest formulari](https://cloud.hangar.org/apps/polls/s/7jUroTqG)

* **Estar al punt d'informació:**
Durant el Sobtec, hem de fer torns per tenir sempre un parell de persones al punt d'informació, però farem tot el possible perquè també puguis seguir les xerrades des d'allà. A més hauràs de mirar que no falti cafè, té, begudes, etc.  
Pots apuntar-te al Punt d'informació en [aquest formulari](https://cloud.hangar.org/apps/polls/s/zgOx8G03)

## Ajuda'ns a fer difusió
També ens pots ajudar fent difusió a través de les xarxes socials: utilitzem el hashtag #SobTec2024. 

## Fer una donació
També pots fer una donació (com a persona o col·lectiu) a la guardiola que posarem al punt d'informació. 

_El SobTec es finança, en gran part, a través d'aquestes donacions_

## Forma part del Grup Promotor

Aquí tens més informació sobre com [formar part del Grup Promotor]({{site.baserul}}/grup-promotor)
