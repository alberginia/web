---
title: "Dispositius mòbils lliures: Reptes i avanços 2024"
layout: programa
ponent: Brian Russell de Degoogled
ponent-description: "Amb seu a Barcelona, Degoogled treballa amb una comunitat creixent per promocionar tecnologies igualitàries, transparents i democràtiques. A més, organitza tallers per  desgooglejar dispositius mòbils i  ofereix telèfons intel·ligents rectificats, és a dir, amb sistemes operatius que respecten la llibertat, privacitat i seguretat de les persones usuàries."
ponent-mastodont:
ponent-link: https://www.degoogled.es/
ponent-twitter: null
draft: false
tags: 
  - smartphone
  - desgooglejar

hour: "15:30"
permalink: /2024/:slug
---

Quines opcions té un consumidor avui dia de maquinari i programari lliure en un dispositiu mòbil? 
En aquesta xerrada, farem un repàs dels pros i contres de SO lliures en dispositius android ([lineage](https://lineageos.org/), [/e/](https://e.foundation), [iodé](https://iode.tech), [microG](https://microg.org/), [CalyxOS](https://calyxos.org/), [GrapheneOS](https://grapheneos.org/) als linux phones, fairphones, etc., punts de venda dels dispositius lliures, etc. Proposarem una sèrie de consells de com trobar un equilibri entre seguretat, privadesa i comoditat i economia segons les necessitats de diferents usuaris. Taller a càrrec de Brian Russell de [degoogled.es]({{page.ponent-link}})
