---
layout: programa
title: "Xerrada artística de Mark Farid i el seu nou projecte Invisible Voice"
ponent: Mark Farid
ponent-description: "Mark Farid és artista, investigador i professor a la universitat de les arts de Londres. S'ha especialitzat en la intersecció entre el món virtual i el físic i l'efecte de les tecnologies tenen sobre les persones. La seva obra parteix de l'ètica hacker i aborda el tema de les polítiques de privacitat, les tecnologies de vigilància advocant per la protecció de dades i la privacitat, convertint-se en una crítica social, legal i política."
ponent-mastodont: 
ponent-link: https://www.markfarid.com/
ponent-twitter: https://twitter.com/morkforid
draft: false
hour: "17:50" 
type: full-afternoon
tags: 
  - 
permalink: /2024/:slug
---

_Invisible Voice_ (IV) és una plataforma que revela les estructures de poder i les pràctiques empresarials darrere dels llocs web amb els que interaccionem. IV busca aprofitar la nostra acció col·lectiva per provocar un canvi significatiu a través de l'empoderament de les dades.

IV actua com una finestra en l'anàlisi de l'ètica i les pràctiques empresarials dels llocs web. IV extreu dades de més de 36 conjunts de dades obertes i fiables, proporcionant als usuaris un tauler de puntuacions al voltant del biaix polític i de lobby, la sostenibilitat ambiental, la responsabilitat corporativa, qüestions de drets humans, estructures de propietat i interessos d'inversió. IV s'integra en els hàbits de navegació de l'usuari, oferint una comprensió dels llocs web que visita. IV permet a les usuàries alinear les seves activitats en línia amb els seus valors ètics, triant boicotejar o recolzar llocs web segons la seva ideologia. IV té com a objectiu reutilitzar les metodologies de publicitat en línia per a un impacte social significatiu en lloc del prioritzar el benefici econòmic, identificant comunitats localitzades al voltant de causes específiques i facilitant la participació, convertint les dades en acció.


Mark Farid va fer una xerrada TEDx el 2017 sobre els seus dos primers projectes _Data Shadow_” (2015) i “Poisonous Antidote” (2016). Farid va ser seleccionat per a la 'New Frontier' Fellowship de l'Institut Sundance a Utah, EUA (2016), pel seu projecte de realitat virtual en curs, “Seeing I”. "Seeing I" es va pilotar com a exposició individual al Festival d'Arts Digitals d'Ars Electronica (2019), i va ser seleccionada per a l'Intercanvi de Residència d'Artistes de Mitjans Europeus, com a part del Programa de Cultures Creatives de la Unió Europea (2020/21).

<hr>

La xerrada serà en anglès, però gràcies al [projecte AINA](https://sobtec.cat/2024/02/18/aina.html), comptarem amb subtítols en català. Per a les preguntes o el debat, anirem traduint les preguntes manualment.

Volem agrair a [Hangar](htttps://hangar.org) i a [HacTe](https://hactebcn.org/) haver fet possible aquesta xerrada i al [projecte Aina](https://projecteaina.cat/tech/) per les traduccions.
