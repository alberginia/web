---
layout: programa
title: "Una intel·ligència artificial Sobirana? Són nostres els datasets, les màquines, i els models que s'utilitzen?"
ponent: Bruno Vianna
ponent-description: |
  Bruno Vianna forma part de <a href="https://canodrom.barcelona/ca/comunitat/digicoria">Digicoria</a>, projecte que busca impulsar la descentralització de la web, a través de tallers, discussions, documentació i producció de material pedagògic.
ponent-mastodont: 
ponent-link: https://canodrom.barcelona/ca/comunitat/digicoria
ponent-twitter: 
draft: false
type: full-morning
hour: "10:45"
permalink: /2024/:slug
tags:
  - intel·ligència artificial
  - datasets
---

En aquesta xerrada explicarem alguns conceptes bàsics perquè tothom pugui entendre què és i fer-se una idea de com funciona la intel·ligència artificial. Això ens permetrà discutir sobre la sobirania i les dependències que es generen al voltant d'aquesta tecnologia així com reflexionar críticament sobre les promeses i els riscos del seu ús per a la participació i transformació social.