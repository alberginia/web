---
layout: programa
title: "Dades obertes per a mapes lliures. Què té OpenStreetMap i què caldria que tingués?"
ponent: Joaquín García Martínez i Dani Möller
ponent-description: ""
ponent-mastodont: https://en.osm.town/@openstreetmap
ponent-link: https://openstreetmap.org/
ponent-twitter: https://www.twitter.com/openstreetmap?lang=ca
draft: false
tags: 
  - mapes
  - cartografia col·laborativa
  - dades obertes

hour: "12:10"
permalink: /2024/:slug
---

L'[OpenStreetMap (OSM)](https://www.openstreetmap.org) és un projecte col·laboratiu per crear mapes de contingut lliure. En aquesta xerrada, es presentarà el projecte i els seus reptes, a més de dades sobre aquesta iniciativa (persones usuàries, elements cartografiats, etc.). Posteriorment, s'abordarà un debat sobre les dades no comercials: OSM ha resultat ser molt útil en oferir dades sobre l'accessibilitat dels espais, sobre la natura urbana o sobre la localització dels desfibril·ladors, que han permès desenvolupar posteriors investigacions. D'altra banda, però el projecte va començar amb l'objectiu de convertir-se en una alternativa oberta als serveis de Google, però alimenta, també, els motors de Overture Maps, una fundació que engloba Microsoft, Meta, TomTom i Amazon. En aquest context, també es discutirà el paper de les administracions públiques davant de les dades obertes (o no tan obertes).