---
layout: programa
title: "Eines digitals per a entitats o projectes: El problema de menystenir el manteniment"
ponent: Procomuns i Víctor Fancelli Capdevila
ponent-description: |
  <a href="https://femprocomuns.coop">Procomuns.coop</a> són una cooperativa que treballa, des del 2017, per consolidar un ecosistema procomú, basat en els principis del cooperativisme obert, l'autogestió comunitària, la sostenibilitat ecològica, econòmica i humana, el coneixement compartit i la replicabilitat.<br/> Víctor Fancelli Capdevila forma part del Grup Promotor per la Sobirania Tecnològica, que organitza el SobTec
ponent-mastodont: 
ponent-link: 
ponent-twitter: 
draft: false
tags:
  - Infraestructura digital
  - manteniment
  
hour: "13:05"
type: 
permalink: /2024/:slug
---

És molt difícil que un col·lectiu pugui desenvolupar la seva feina sense eines digitals, per compartir la informació necessària per desenvolupar el seu projecte. Per a molts projectes, però, és un problema utilitzar una infraestructura digital (el conjunt d'eines digitals) que no contradigui les seves idees: per desconeixement o per motius econòmics, utilitzen eines aparentment gratuïtes sense adonar-se que delegar el manteniment d'aquesta infraestructura  implica establir una dependència amb les empreses i el seu model de negoci que hi ha al darrere. A la xerrada s'explicarà quins són els problemes de menystenir el manteniment de les infraestructures digitals i quines alternatives existeixen.

