---
layout: default
title: "Grup Promotor"
---

## El Grup Promotor del Congrés de Sobirania Tecnològica

El **Grup Promotor per la Sobirania Tecnològica** és qui organitza el Congrés de Sobirania Tecnològica. Volem que aquest grup sigui la llavor d'un espai estable per aglutinar i teixir alternatives en aquest àmbit.

Les reunions preparatòries del congrés es fan els dimarts al vespre (al voltant de les 20:00-20:30) per videoconferència. Si vols participar en les pròximes reunions envia un correu electrònic a: <a href="mailto:{{ site.email }}">{{site.email}}</a>.

Us animem a participar en l'organització del congrés, en la mesura que cadascú se senti més còmode: formant part del Grup Promotor, organitzant o proposant algun taller o xerrada, proposant talleristes o fent-nos arribar els vostres comentaris i propostes. El SobTec es finança a partir de donacions que es recullen durant la mateixa jornada.

Poc abans del congrés farem una crida per a totes aquelles persones que ens puguin ajudar el dia del congrés: hi haurà tota mena de tasques com ara (logístiques, muntar cadires, preparar sales, etc.), ajudar-nos a prendre apunts, ajudar amb la gravació audiovisual, etc. Segur que trobem alguna tasca per a tu!

