---
layout: default
title: "Edicions anteriors"
---

<div class="edicions-anteriors">
{% for item in site.data.edicions_anteriors %}
 <h1><a href="/{{item.any}}">Congrés {{item.any}}</a></h1>
{% endfor %}
</div>
