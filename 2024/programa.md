---
layout: default
title: "Programa"
---
<h1>Programa</h1>

{% for post in site.posts %}
{% if post.type == "full-morning" %}
<div class="agenda-item">
<h2 class="time">{{post.hour}}</h2>
<div class="talks" style="width:100%;">
                    <div class="talk">
                    <a href="{{post.url}}"><h2>{{post.title}}</h2></a>
                    <p>{{ post.excerpt }}</p>
                    <hr />
                    </div>    
                </div>
</div>

{% endif %}
{%endfor %}
<div class="frame">
{% assign talks = site.posts | sort_natural: "hour" %}
<div class="base">
    <h2> Sala polivalent </h2>
    {% for talk in talks %} 
        <div class="agenda-item"> 
    
        {% if talk.type != "full-morning" and talk.type!="full-afternoon" and talk.room != "Sala petita"%}
        <h2 class="time">{{talk.hour}}</h2>
                <div class="talks">
                    <div class="talk">
                    <a href="{{talk.url}}"><h2>{{talk.title}}</h2></a>
                    <p>{{ talk.excerpt }}</p>
                    {% unless talk.hour == "15:30" %}<hr />{%endunless%}
                    </div>    
                </div>
          {% endif %}

        </div>
    {% endfor %}
</div>

<div class="smallroom">
<h2> Sala Petita </h2>
    {% for talk in talks %} 
        {% if talk.room == "Sala petita" %}
            <div class="talk talk-side">
                <h3><a href="{{ talk.url }}">{{talk.title }}</a></h3>
                <p>{{ talk.excerpt }}</p>
            </div>

        {% endif %}
    {% endfor %}
</div>
</div>

<div class="linia">
    <div class="agenda-item">
        <div class="talks" style="width:100%">
            <div class="talk">
            <hr/>
            </div>
        </div>
    </div>

    {% for conf in site.posts %}
    <div class="agenda-item"> 
        {% if conf.type == "full-afternoon" %}
        <h2 class="time">{{conf.hour}}</h2>
            <div class="talks" style="width:100%">
            <div class="talk">
                <h2><a href="{{ conf.url }}">{{conf.title }}</a></h2>
                <p>{{ conf.excerpt }}</p>
                <hr />
            </div>
            </div>

        {% endif %}
    </div>
    {% endfor %}
    
</div>
