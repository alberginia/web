---
layout: default
title: "Butlletí"
---

# Butlletí de Congrés

Si vols rebre informació actualitzada del pròxim Congrés de la Sobirania Tecnològica només cal que t'inscriguis al nostre enviant un mail a <info@sobtec.cat> o mitjançant el

<div style="text-align:center">
<a href="https://mlists.pangea.org/subscription/AwtBDMwaHL" class="button">Formulari d'alta del butlletí</a>
</div>

## Què passa amb les meves dades?
Les dades que et demanem són el teu correu electrònic i, si vols, nom i cognom(s). Demanem aquestes dades per poder-te enviar el butlletí i informar-te de convocatòries i notícies relacionades amb el SobTec: per això enviem pocs mails, en general, durant la tardor (quan comencem a organitzar-ho tot) i poc abans del Congrés. 

Les teves dades no seran cedides a tercers, i les conservarem fins que demanis donar-te de baixa del butlletí (pots fer-ho mitjançant un enllaç al final de cada butlletí o enviant un mail a <info@sobtec.cat>).

El responsable del tractament de les dades recollides és:  
Víctor Fancelli Capdevila  
Werderstraße, 33  
76137 Karlsruhe  
victor@sobtec.cat