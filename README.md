# Website of SobTec

This project is  a sum of different pages using [jekyll](https://jekyllrb.com/). 

⚠️ Every year is a separate Jekyll project **with a different Jekyll version!** ⚠️

[.gitlab-ci.yml](.gitlab-ci.yml) will only generate the page from last year; other years' websites are simply stored as static websites (in /public)

To run locally:

1. Prepare your computer for [jekyll requirements](https://jekyllrb.com/docs/installation/)
2. Install jekyll and bundler gems
```
gem install jekyll bundler
```

3. Clone this repository
```
git clone git@gitlab.com:grup_promotor_sobtec/web.git
```

4. Enter the year's folder (here, 2024)
```
cd web/2024
```

5. Install bundler
```
bundle install
```

6. And here we are! Finally
```
bundle exec jekyll serve --incremental
```

## Creating a new year's Sobtec

1. Copy from last year
```
cp 2024 2025
``` 

2. Check Jekyll's version
``` jekyll -v ``` , and if necessary, adapt the `Gemfile` (in new year's folder!)

3. Add previous year in `_data/edicions_anteriors` and change the year in `gitlab-ci.yml` (in the project's root foler).

4. For the colors, change `$primary-year-color` and `$secondary-year-color` in `/assest/css/main.scss`

4. Comment everything... and change!