---
layout: default
title: "Edicions anteriors"
---

<div>
{% for item in site.data.edicions_anteriors %}
 <h1><a href="/{{item.any}}" style="color:#9E186E">Congrés {{item.any}}</a></h1>
{% endfor %}
</div>
