---
layout: default
title: "Butlletí"
---

## Butlletí de Congrés

Si vols rebre informació actualitzada del proper Congrés només cal que t'inscriguis al nostre butlletí des de aquest [formulari](https://lists.bcnfs.org/mailman/listinfo/sobtec) o enviant un mail a info@sobtec.cat.

Ens comprometem a només enviar informació relacionada amb el congrés.

Gràcies a [Barcelona Free Software](https://bcnfs.org/) per allotjar en nostre butlletí en la seva instancia de [mailman](https://www.list.org/)
